Telegram-amoCRM integration.

### install

- install system dependencies

`
sudo apt install tesseract-ocr libleptonica-dev ffmpeg libsm6 libxext6 libmagickwand-dev -y
`

- config ImageMagick
  
    find a string in /etc/ImageMagick-6/policy.xml

    ` 
    <policy domain="coder" rights="none" pattern="PDF" />
    `

    and replace it to

    `
    <policy domain="coder" rights="read | write" pattern="PDF" />
    `


 - download tesseract Russian test data

`
wget https://github.com/tesseract-ocr/tessdata/raw/master/rus.traineddata
`

- move tesseract data to system folder

`
sudo mv rus.traineddata /usr/share/tesseract-ocr/4.00/tessdata/rus.traineddata
`

- install python dependencies

`
pip install -r requerements.txt
`
